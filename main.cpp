#define INF 1000000000             //константа, на которую заменяются отсутствующие ребра графа, для работы алг Дейкстры
#include <iostream>

#include <myGraph.h>
#include <myGraph.cpp>
#include <readGraphFromData.cpp>

using namespace std;

int main() {
    string pathToData = "C:/Users/HYPER/myGraph/data.txt";
    int nodeCount =  spotNodeCount(pathToData);
    int** data = readGraph(pathToData);
    vector<string> values = {"a", "b", "c", "d", "e", "f"};

    myGraph<string> A(data, nodeCount, values);
    A.show();

    vector<int> inp = {0, 0, 0, 0, 4, 5};
    A.addNode(inp, "g");
    A.show();

    A.deleteNodeByValue("g");
    A.show();

    A.delLink(0, 3);
    A.show();

    A.changeLink(0, 3, 7);
    A.show();

    vector<int> dist = A.findMinDist(0);
    for (size_t i = 0; i < dist.size(); i++) {
        cout << "min dist to node " << i << "(" << values[i] << ") is " << dist[i] << endl;
    }
    cout << endl;

    vector<int> path = A.findMinPath(0, 3);
    for (size_t i = path.size(); i > 0; --i) {
        cout << path[i - 1] << "(" << values[i - 1] << ") ";
    }
    cout << endl << endl;

    A.contractionNodes(1, 3);
    A.show();
}
