#define INF 1000000000    //константа, на которую заменяются отсутствующие ребра графа, для работы алг Дейкстры
#include "myGraph.h"

#include <iostream>
#include <string>

using namespace std;

template<class T>
myGraph<T>::myGraph(int** inpData, int inpNodeCount, vector<T> inpValues) {
    data = inpData;
    nodeCount = inpNodeCount;
    values = inpValues;
}

template<class T>
int myGraph<T>::findByValue(T value){
    for (int i = 0; i < nodeCount; i++) {
        if (values[i] == value) return i;
    }
    return INF;
}

template<class T>
void myGraph<T>::addNode(vector<int> links, T value) {
    values.push_back(value);
    nodeCount++;
    int** newData = new int*[nodeCount];
    for (int i = 0; i < nodeCount; i++) {
        newData[i] = new int[nodeCount];
    }

    for (int i = 0; i < nodeCount - 1; i++) {
        for (int k = 0; k < nodeCount - 1; k++) {
            newData[i][k] = data[i][k];
        }
    }

    for (size_t i = 0; i < links.size(); i++) {
        if (links[i] != 0) {
            newData[nodeCount - 1][i] = links[i];
            newData[i][nodeCount - 1] = links[i];
        }
        else {
            newData[nodeCount - 1][i] = INF;
            newData[i][nodeCount - 1] = INF;
        }
    }
    newData[nodeCount - 1][nodeCount - 1] = INF;

    data = newData;
}

template<class T>
void myGraph<T>::deleteNodeByIndex(int delNode) {
    values.erase(values.begin() + delNode);
    int** newData = new int*[nodeCount - 1];
    for (int i = 0; i < nodeCount - 1; i++) {
        newData[i] = new int[nodeCount - 1];
    }

    int curX = 0;
    int curY = 0;
    for (int i = 0; i < nodeCount; i++) {
        if (i == delNode) continue;
        curY = 0;
        for (int k = 0; k < nodeCount; k++) {
            if (k == delNode) continue;
            newData[curX][curY] = data[i][k];
            curY++;
        }
        curX++;
    }

    data = newData;
    nodeCount--;
}

template<class T>
void myGraph<T>::deleteNodeByValue(T value) {
    this->deleteNodeByIndex(this->findByValue(value));
}

template<class T>
void myGraph<T>::contractionNodes(int node1, int node2) {
    try {
        if (data[node1][node2] == INF) throw 1;
    }
    catch(int a) {
        cout << "Nodes not connected(" << a << ")"  << endl << endl;
    }

    int minNode;
    int maxNode;
    if (node1 < node2) {
        minNode = node1;
        maxNode = node2;
    }
    else {
        minNode = node2;
        maxNode = node1;
    }
    for (int i = 0; i < nodeCount; i++) {
        if ((data[maxNode][i] != INF) && (data[maxNode][i] < data[minNode][i])) {
            changeLink(minNode, i, data[maxNode][i]);
        }
    }

    deleteNodeByIndex(maxNode);
}

template<class T>
void myGraph<T>::delLink(int node1, int node2)
{
    data[node1][node2] = INF;
    data[node2][node1] = INF;
}

template<class T>
void myGraph<T>::changeLink(int node1, int node2, int value)
{
    if (value != 0) {
        data[node1][node2] = value;
        data[node2][node1] = value;
    }
    else {
        data[node1][node2] = INF;
        data[node2][node1] = INF;
    }
}

template<class T>
vector<int> myGraph<T>::findMinDist(int startNode) {
    vector<int> dist(nodeCount, INF);
    dist[startNode] = 0;
    vector<bool> isVisited(nodeCount);
    int min_dist = 0;
    int min_vertex = startNode;
    while (min_dist < INF)
    {
        int i = min_vertex;
        isVisited[i] = true;
        for (int j = 0; j < nodeCount; j++)
            if (dist[i] + data[i][j] < dist[j])
                    dist[j] = dist[i] + data[i][j];
        min_dist = INF;
        for (int j = 0; j < nodeCount; j++)
            if (!isVisited[j] && dist[j] < min_dist)
            {
                min_dist = dist[j];
                min_vertex = j;
            }
    }

    return dist;
}

template<class T>
vector<int> myGraph<T>::findMinPath(int startNode, int finishNode) {
    vector<int> dist = this->findMinDist(startNode);
    vector<int> path;
    int curWeight = dist[finishNode];
    path.push_back(finishNode);
    while (curWeight > 0) {
        for (int i = 0; i < nodeCount; i++) {
            if (data[path.back()][i] != INF){
                if (dist[path.back()] == (dist[i] + data[path.back()][i])) {
                    path.push_back(i);
                    curWeight = dist[path.back()];
                }
            }
        }
    }

    return path;
}

template<class T>
void myGraph<T>::show() {
    cout << " ";
    for (int i = 0; i < nodeCount; i++) {
        cout << values[i] << " ";
    }
    cout << endl;
    for (int i = 0; i < nodeCount; i++) {
        cout << values[i];
        for (int k = 0; k < nodeCount; k++) {
            if (data[i][k] != INF) {
                cout << data[i][k] << " ";
            }
            else {
                cout << 0 << " ";
            }
        }
        cout << endl;
    }
    cout << endl;
}
