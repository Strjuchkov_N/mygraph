#include <vector>
#ifndef MYGRATH_H
#define MYGRATH_H

template<class T>
class myGraph {
public:
    int** data;                                                     //матрица смежности графа

    std::vector<T> values;

    int nodeCount;                                                  //кол-во вершин графа

    myGraph(int** inpData, int inpNodeCount, std::vector<T>inpValues);//конструктор

    int findByValue(T value);                                       //возвращает индекс вершины с заданым значением

    void addNode(std::vector<int> links, T value);                  //добавление вершины

    void deleteNodeByIndex(int delNode);                                   //удаление вершины по индексу

    void deleteNodeByValue(T value);                                       //удаление вершины по значению

    void contractionNodes(int node1, int node2);                    //стягивание двух вершин

    void delLink(int node1, int node2);                             //удаление ребра

    void changeLink(int node1, int node2, int value);               //изменение веса ребра, в т.ч. можно удалять или добавлять ребра

    std::vector<int> findMinDist(int startNode);                    //находит минимальные расстояния до всех вершин от заданой(алг Дейкстры), возвращает вектор
                                                                    //значений, в котором каждому индексу соответствует вершина
    std::vector<int> findMinPath(int startNode, int finishNode);    //находит минимальный путь между двумя вершинами, возвращает вектор со списком всех вешин,
                                                                    //содержащихся в нем
    void show();                                                    //вывод графа в виде матрицы смежности в консоль
};

#endif // MYGRATH_H
