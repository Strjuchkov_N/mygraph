#-------------------------------------------------
#
# Project created by QtCreator 2016-12-23T18:11:19
#
#-------------------------------------------------

QT       += core

QT       -= gui

QMAKE_CXXFLAGS += -std=c++11

TARGET = myGraph
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    readGraphFromData.cpp \
    myGraph.cpp

OTHER_FILES += \
    data.txt

HEADERS += \
    myGraph.h
