//файл data.txt содержит матрицу связанности и выступает в каестве списка инициализации для графа, в процессе работы изменения в него не заносятся

#define INF 1000000000          //константа, на которую заменяются отсутствующие ребра графа, для работы алг Дейкстры
#include <fstream>
#include <string>

using namespace std;

int spotNodeCount(string path){            //открывает файл с исходной матрицей смежности и возвращает кол-во вершин графа
    string dataStr;
    ifstream dataR;
    int nodeCount = 0;

    dataR.open(path);
    getline(dataR, dataStr);
    for (size_t i = 0; i < dataStr.size(); i++) {
        if (dataStr[i] == ' ') nodeCount++;
    }
    nodeCount++;
    dataR.close();

    return nodeCount;
}

int** readGraph(string path){             //открывает файл с исходной матрицей смежности и возвращает ее в виде массива
    int nodeCount = spotNodeCount(path);
    string dataStr;
    ifstream dataR;

    int** connectMatrix = new int*[nodeCount];
    for (int i = 0; i < nodeCount; i++) {
            connectMatrix[i] = new int[nodeCount];
    }

    dataR.open(path);
    for(int countX = 0; countX < nodeCount; countX++) {
        getline(dataR, dataStr);
        int countY = 0;
        string num = "";
        for (size_t k = 0; k < dataStr.size(); k++) {
            if ((dataStr[k] == ' ') || (k + 1 == dataStr.size())) {
                if (k + 1 == dataStr.size()) num += dataStr[k];
                if (stoi(num) == 0) {
                    connectMatrix[countX][countY] = INF;
                } else {
                    connectMatrix[countX][countY] = stoi(num);
                }
                num = "";
                countY++;
            }
            else {
                num += dataStr[k];
            }
        }
    }
    dataR.close();
    return connectMatrix;
}
